<?= $js_inline ?>
<div class="container_success">
    <div class="image_result">
        <img src="<?= $thumb_url ?>" /> 
    </div>
    <div class="content">
        <h2><?= t('Done!') ?></h2>
        <?= t('At the left, you see the image you just uploaded. Make sure it is the correct one and click "insert".') ?>
        <?= $controls ?>
    </div>
</div>