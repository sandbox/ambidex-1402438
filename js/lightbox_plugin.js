(function() {
    
    tinymce.create('tinymce.plugins.lightbox_plugin', {
        init : function(ed, url) {
            
            // Register commands
            ed.addCommand('mceLightbox_plugin', function() {
                ed.windowManager.open({
                    file : '/admin/lightbox_plugin/dashboard',
                    width : 580,
                    height : 480,
                    inline : true,
                    scrollbars : 1,
                    popup_css : false
                }, {
                    plugin_url : url
                });
            });

            // Register buttons
            ed.addButton('lightbox_plugin', {
                title : 'Add Lightbox image',
                cmd : 'mceLightbox_plugin',
                image : url + '/../images/lightbox_plugin.png'
            });

        },

        getInfo : function() {
            return {
                longname : 'Lightbox wysiwyg plugin',
                author : 'Niels van Aken - 10uur communicatie en creatie',
                authorurl : 'http://www.10uur.nl/',
                infourl : 'http://drupal.org/project/lightbox_wysiwyg',
                version : tinymce.majorVersion + "." + tinymce.minorVersion
            };
        }
    });

    // Register plugin
    tinymce.PluginManager.add('lightbox_plugin', tinymce.plugins.lightbox_plugin);
})();