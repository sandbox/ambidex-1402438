var LightboxpluginDialog = {
    init : function () {
        var ed = tinyMCEPopup.editor;
    },
    insertImage : function () {
        tinyMCEPopup.execCommand("mceInsertContent", false, '<a href="' + Drupal.settings.lightboxdash.full_url + '"><img src="' + Drupal.settings.lightboxdash.thumb_url + '" rel="lightbox" /></a>');
        tinyMCEPopup.close();
    }
} 


tinyMCEPopup.onInit.add(LightboxpluginDialog.init, LightboxpluginDialog);

$(document).ready(function () {
    Drupal.behaviors.lightboxdash = function () {
        $('input#edit-insert').click (function (e) { 
            e.preventDefault();
            LightboxpluginDialog.insertImage();
        });
        $('input#edit-cancel').click (function (e) {
            e.preventDefault();
            tinyMCEPopup.close();
        });
    }
    Drupal.attachBehaviors();
});
