<?php

function _lightbox_dashboard_page() {
    module_invoke('admin_menu', 'suppress');
    // Add CSS.
    drupal_add_css(drupal_get_path('module', 'lightbox_wysiwyg') . '/css/lightbox_dashboard.css');

    // Add JS
    static $defualt_settings_added = FALSE;

    // Add default settings
    if (!$defualt_settings_added) {
        $defualt_settings_added = TRUE;

        $settings = array(
            'linkit' => array('ajaxcall' => url('admin/lightbox/geteditresult')),
        );

        drupal_add_js($settings, 'setting');
    }
    

    // @todo Should include a switch here to check for the editor, but for now, only tinymce support.
    drupal_add_js(wysiwyg_get_path('tinymce') . '/jscripts/tiny_mce/tiny_mce_popup.js');
    drupal_add_js(drupal_get_path('module', 'lightbox_wysiwyg') . '/js/lightbox_dashboard.js');

    print theme('lightbox_dashboard', drupal_get_form('_lightbox_form'));
    exit();
}

/**
 * The lightbox dashboard form
 */
function _lightbox_form() {
    // Load the presets that have been made available
    $thumbnail_presets = variable_get('lightbox_wysiwyg_image_thumb_sizes', null);
    $full_presets = variable_get('lightbox_wysiwyg_image_full_sizes', null);
    
    // Check whether we should present the presets at all. If there are only one preset for both thumb and full available, we do not need to show the form.
    $show_thumbnail_presets = false;
    $show_full_presets = false;
    if (!empty($thumbnail_presets)) {
        if (!empty($full_presets)) {
            if (count ($thumbnail_presets) > 1) {
                $show_thumbnail_presets = true;
            }
            if (count ($full_presets) > 1) {
                $show_full_presets = true;
            } 
        } 
        else {
            watchdog('lightbox_wysiwyg', t('No presets have been selected for full images! You need to <a href="/admin/settings/lightbox_plugin">select the presets</a> in the settings page before you can continue'), null, WATCHDOG_ERROR);
            exit(t('Please contact your administrator, some settings should be set.'));
        }
    }
    else {
        watchdog('lightbox_wysiwyg', t('No presets have been selected! You need to <a href="/admin/settings/lightbox_plugin">select the presets</a> in the settings page before you can continue'), null, WATCHDOG_ERROR);
        exit(t('Please contact your administrator, some settings should be set.'));
    }
    
    
    $form['#attributes'] = array('enctype' => "multipart/form-data");
    $form['#submit'] = array('_lightbox_form_submit');
    $form['image'] = array(
        '#type' => 'file',
        '#title' => t('Upload lightbox image'),
        //'#size' => 40,
    );
    
    if ($show_full_presets === true || $show_thumbnail_presets === true) {
        $form['sizes'] = array( 
            '#type' => 'fieldset',
            '#collapsible' => true,
            '#collapsed' => true,
            '#title' => t('Image manipulations'),
            '#tree' => true
        );
    }
    else {
        $form['sizes'] = array( 
            '#tree' => true
        );
    }

    // If we need to show the presets, we show a select field. If not, we only place the default values.
    if ($show_thumbnail_presets === true) { 
        $form['sizes']['thumb'] = array (
            '#title' => t('Thumbnail size'),
            '#type' => 'select',
            '#description' => t('The preset manipulation of the thumbnail.'),
            '#default_value' => variable_get('lightbox_wysiwyg_image_thumb_default', null),
            '#options' => $thumbnail_presets
        );
    }
    else {
        $form['sizes']['thumb'] = array(
            '#type' => 'hidden',
            '#value' => current($thumbnail_presets)
        );
    }

    if ($show_full_presets === true) { 
        $form['sizes']['full'] = array (
            '#title' => t('Lightbox popup size'),
            '#type' => 'select',
            '#description' => t('The preset image manipulation of the lightbox popup image.'),
            '#default_value' => variable_get('lightbox_wysiwyg_image_full_default', null),
            '#options' => variable_get('lightbox_wysiwyg_image_full_sizes', null)
        );
    }
    else {
        $form['sizes']['full'] = array(
            '#type' => 'hidden',
            '#value' => current($full_presets)
        );
    }
    

    $form['link']['cancel'] = array(
        '#type' => 'button',
        '#value' => t('Cancel'),
        '#weight' => 10,
    );

    $form['link']['upload'] = array(
        '#type' => 'button',
        '#value' => t('Upload'),
        '#weight' => 11,
        '#ahah' => array(
            'event' => 'click',
            'path' => 'admin/lightbox_plugin/dashboard/process_file',
            'wrapper' => 'form-wrapper',
            'method' => 'replace',
            'effect' => 'fade',
            'progress' => array(
                'type' => 'bar',
                'message' => t('Loading...')
            )
        ),
    );
    
    return $form;
}

function lightbox_wysiwyg_settings_page () {
    
    $output .= drupal_get_form('_lightbox_settings_form');
    
    return $output;
}

function _lightbox_settings_form() {
    $imagecache_presets = imagecache_presets();
    // Build options for the select fields, populated with imagecache presets
    if (!empty($imagecache_presets)) {
        $preset_options = array();
        foreach ($imagecache_presets as $preset) {
            $preset_options[$preset['presetname']] = $preset['presetname'];
        }
    }
    
    $form['#submit'] = array('_settings_save');
    
    $form['image'] = array (
        '#type' => 'fieldset',
        '#title' => 'Image settings',
        '#tree' => true
    );
    
    $form['image']['thumb'] = array (
        '#type' => 'fieldset',
        '#title' => 'Thumbnails'
    );
    
    $form['image']['full'] = array (
        '#type' => 'fieldset',
        '#title' => 'Lightbox popup image'
    );
    
    $form['image']['thumb']['sizes'] = array(
        '#type' => 'select',
        '#multiple' => true,
        '#title' => t('Thumbnail image presets to be available.'),
        '#description' => t('When the user uploads a new image, he will be able to select an off-default size to make the thumbnails. If none of the presets are selected, all the presets will be available. If only one is selected, the user will not get any option to select a size.'),
        '#default_value' => variable_get('lightbox_wysiwyg_image_thumb_sizes', null),
        '#options' => $preset_options
    );
    
    $form['image']['thumb']['default'] = array(
        '#type' => 'select',
        '#title' => t('The default thumbnail image preset. (this setting is ignored when using only one preset)'),
        '#description' => t('The value to be default.'),
        '#default_value' => variable_get('lightbox_wysiwyg_image_thumb_default', null),
        '#options' => $preset_options
    );
    
    $form['image']['full']['sizes'] = array(
        '#type' => 'select',
        '#multiple' => true,
        '#title' => t('Lightbox popup image preset to be available'),
        '#description' => t('When the user uploads a new image, he will be able to select an off-default size to make the lightbox popup image. If none of the presets are selected, all the presets will be available. If only one is selected, the user will not get any option to select a size.'),
        '#default_value' => variable_get('lightbox_wysiwyg_image_full_sizes', null),
        '#options' => $preset_options
    );
    
    $form['image']['full']['default'] = array(
        '#type' => 'select',
        '#title' => t('The default lightbox popup image preset.'),
        '#description' => t('The value to be default. (this setting is ignored when using only one preset)'),
        '#default_value' => variable_get('lightbox_wysiwyg_image_full_default', null),
        '#options' => $preset_options
    );
    
    $form['buttons']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save')
    );
    
    return $form;
}

function _settings_save ($form, &$form_state) {
    
    variable_set('lightbox_wysiwyg_image_thumb_sizes', $form_state['values']['image']['thumb']['sizes']);
    variable_set('lightbox_wysiwyg_image_thumb_default', $form_state['values']['image']['thumb']['default']);
    variable_set('lightbox_wysiwyg_image_full_sizes', $form_state['values']['image']['full']['sizes']);
    variable_set('lightbox_wysiwyg_image_full_default', $form_state['values']['image']['full']['default']);
    drupal_set_message(t('Your settings have been saved.'));
}


?>
